# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

# Using Win/Cmd as mod key
set $mod Mod4
# Alacritty as default terminal
set $term alacritty

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# Start XDG autostart .desktop files using dex. See also
# https://wiki.archlinux.org/index.php/XDG_Autostart
exec --no-startup-id dex --autostart --environment i3

# The combination of xss-lock, nm-applet and pactl is a popular choice, so
# they are included here as an example. Modify as you see fit.

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork

# NetworkManager is the most popular way to manage wireless networks on Linux,
# and nm-applet is a desktop environment-independent system tray GUI for it.
exec --no-startup-id nm-applet

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+Shift+q kill

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# Colors ###############################
# Main colors
set $bg #1d1f21
set $fg #c5c8c6

# Common colors
set $black   #282a2e
set $red     #a54242
set $green   #8c9440
set $yellow  #de935f
set $blue    #5f819d
set $magenta #85678f
set $cyan    #5e8d87
set $white   #707880

# Bright colors
set $br_black   #373b41
set $br_red     #cc6666
set $br_green   #b5bd68
set $br_yellow  #f0c674
set $br_blue    #81a2be
set $br_magenta #b294bb
set $br_cyan    #8abeb7
set $br_white   #c5c8c6

# Settings ###############################
# Windows settings
smart_borders on
default_border pixel 1
default_floating_border pixel 1
floating_minimum_size 80 x 24
floating_maximum_size 1280 x 800

# Window title font
font pango:basis33 12

# Gaps
#gaps vertical 38
#gaps horizontal 8
#gaps inner 2

# Colors			Border   Background   Text
client.focused			$green   $green	      $fg
client.unfocused		$bg	 $bg	      $fg
client.focused_inactive		$bg	 $green       $bg
client.urgent			$green	 $green	      $fg	
client.background $bg

# Startup ################################
# Set startup volume
exec pactl set-sink-volume @DEFAULT_SINK@ 40%
# Set startup brightness
exec brightnessctl set 50%
# Disable beeps
exec xset -b
# DPMS monitor setting (standby|suspend|poweroff)
exec xset dpms		  600     900     1200
# Keyboard layout
exec_always setxkbmap -layout "us,ru" -variant "mac,mac" -option "grp:alt_shift_toggle"
# Launch polybar
exec_always --no-startup-id $HOME/.config/polybar/launch.sh
# Set wallpaper
exec_always --no-startup-id nitrogen --set-zoom-fill ~/Pictures/wallpaper-lunix.jpg

# Function Keys ##########################
# Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle

# Screen brightness controls
bindsym XF86MonBrightnessUp exec --no-startup-id brightnessctl set +10%
bindsym XF86MonBrightnessDown exec --no-startup-id brightnessctl set 10%-

# Keyboard backlight controls
bindsym XF86KbdBrightnessUp exec sudo kbd-backlight total
bindsym XF86KbdBrightnessDown exec sudo kbd-backlight off

# Hotkeys ################################
bindsym $mod+Return exec $term
bindsym $mod+d exec --no-startup-id rofi -show run
bindsym $mod+Shift+d exec --no-startup-id rofi -show window

# Infinite wisdom
bindsym $mod+Shift+w exec --no-startup-id notify-send "$(fortune | cowsay)"

# Applications quick access
mode "launch" {
	bindsym t exec telegram-desktop
	bindsym w exec firefox		
	bindsym m exec thunderbird	
	bindsym p exec flameshot gui	
	bindsym f exec pcmanfm		
	bindsym c exec codium		
	bindsym h exec $term --command htop

	# Help message
	set $help notify-send "$(cat ~/.config/i3/launch.help)"
	bindsym Shift+h exec --no-startup-id $help

	# Back to normal
	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $mod+Shift+f mode "default"
}

# Enter application launch mode
bindsym $mod+Shift+f mode "launch"

