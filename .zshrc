# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Use powerline
USE_POWERLINE="true"
# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi

# User variables
export EDITOR=vim

# User aliases
alias e=${EDITOR}
alias cls="clear"
alias zshconf="e ~/.zshrc"
alias i3conf="e ~/.config/i3/config"
alias pls="sudo" # ROFL
alias wisdom="fortune | cowsay"

## 'ls' aliases
alias l=ls
alias la="ls -a"
alias ll="ls -l"

## 'pacman' aliases
alias pm="pacman"
alias spm="sudo pacman"
alias pms="sudo pacman -S"
alias pmr="sudo pacman -R"
alias pmk="sudo pacman -Rs"
alias pmu="sudo pacman -Sy"
alias pmq="pacman -Q"
alias pmd="pacman -Qs"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
