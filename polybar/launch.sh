#!/usr/bin/env zsh

# Terminate already running bar instances
killall -q polybar

# Launch configured bars
echo "Starting polybar..."
polybar --config=~/.config/polybar/include/bar/main.ini main 2>&1 | tee -a ~/.config/polybar/log/main.log & disown
echo "Polybar started!"

